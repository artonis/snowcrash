import java.util.Arrays;
import java.nio.file.*;
import java.nio.charset.*;
import java.io.*;

public class Main {
    public static void main(String[] args) throws Exception {
	Path path = Paths.get(args[0]);
	byte[] array = Files.readAllBytes(path);
        for(int i =0; i<array.length; i++) {
     		array[i] = (byte) (array[i] - i);
        }
	System.out.println(new String(array, Charset.forName("UTF-8")));
    }
}

