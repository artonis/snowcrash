import java.io.*;
import java.util.Scanner;

public class Main {
	public static void main(final String args[]) throws FileNotFoundException {
		final String fileName = args[0];

		final InputStream inputStream = new FileInputStream(fileName);
		final Scanner scanner = new Scanner(inputStream);
		while(scanner.hasNextLine()) {
			process(scanner.nextLine());
		}
	}

	private static void process(final String text) {
		final String info = getInfo(text);
		final String[] chars = info.split(":");
		for (final String symbol : chars) {
			final int value;
			try {
				value = valueOf(symbol);
			} catch (final Exception e) {
				System.out.print("'*'");
				continue;
			}
			if ((value > 32) && (value < 127))
				System.out.print((char)value);
			else
				System.out.print("'" + value + "'");
		}
		System.out.println();
	}

	private static String getInfo(final String text) {
		final String[] split = text.split("\"");
		return split[3];
	}

	private static int valueOf(final String symbol) throws Exception {
		final char[] chars = symbol.toCharArray();
		return 16 * convert(chars[0]) + convert(chars[1]);
	}

	private static int convert(char value) throws Exception {
		if (Character.isDigit(value))
			return value - '0';
		if (value < 'a' || value > 'f')
			throw new Exception();
		return 10 + value - 'a';
	}
}
